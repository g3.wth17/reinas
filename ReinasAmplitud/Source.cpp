#include <iostream>
#include <vector>
#include <queue>
#include "Tablero.h"
using namespace std;
#define TAM 6
int main()
{
	// Espacio de Estado 0 
	// ESTA ES LA SEMILLA!! :D
	int vectorR[TAM];
	vectorR[1] = 0;
	vectorR[2] = 0;
	vectorR[3] = 0;
	vectorR[4] = 0;
	vectorR[5] = 0;
	Tablero semilla;
	
	semilla.mandarReinas(vectorR);
	// CREAMOS LOS NODOS VACIOS!!...
	vector<Tablero> tableros;
	tableros.push_back(semilla);
	bool resp = false;
	int NumeroDeNodosVisitados = 0;
	int i = 0;
	//EMPEZAMOS LA BUSQUEDA MAS LARGA DE LA HISTORIA..........
	while (tableros.size() != 0 && resp!=true)
	{
		tableros[i].depurarPosiciones();
		if (tableros[i].ValidosMayoresAReinas())
		{
			//CORAZON DEL PROGRAMA!!>..
			while (tableros[i].getTamValidos() != 0 && resp != true)
			{
				tableros[i].SacarSiguientePosibilidad();
				for (int j = 1; j < TAM; j++)
				{
					vectorR[j] = tableros[i].devolverVectorR(j);
				}
				Tablero nuevo;
				nuevo.mandarReinas(vectorR);
				nuevo.MostrarTablero();
				cout << endl;
				if (nuevo.EsSolucion())
				{
					resp = true;
					cout << "Es Solucion" << endl;
					nuevo.MostrarTablero();
					cout << endl;
				}
				else
				{
					int Iterador = 0;
					bool resp = true;
					while (Iterador < tableros.size() && resp != false)
					{
						if (tableros[Iterador].NoRepetido(vectorR))
						{
							resp = false;
						}
						Iterador++;
					}
					if (resp == true)
					{
						tableros.push_back(nuevo);
					}
				}
			}
		}
		NumeroDeNodosVisitados++;
		//ELIMINAMOS EL QUE NO NOS IMPORTA
		tableros.erase(tableros.begin());
	
	}
	cout << NumeroDeNodosVisitados << endl;
	system("pause");
	return 0;
}

